import styled from "styled-components";

export const StyledHistory = styled.div`
  .historycontent {
    margin: 10% 5% 0 5%;
    font-family: "MontserratMedium", sans-serif;
  }

  .year {
    font-size: 125%;
  }

  .activity {
    margin: 5% 0 5% 0;
    display: flex;
    justify-content: space-between;
    align-items: center;
    content: "";
  }

  .records {
    margin: 0 0 0 2.5%;
    padding: 0 0 0 2.5%;
    border-left: solid black 8px;
    width: 40%;
    font-size: 100%;
    line-height: 150%;
  }
  .image {
    width: 47.5%;
  }
  .records p {
    font-size: 110%;
  }
  .records h1 {
    line-height: 150%;
  }
`;
