import React from "react";
import { StyledGalery } from "./style";
import Navbar from "../../component/Navbar";
import PageTitle from "../../component/PageTitle";
import ImageList from "./imageList";

const Gallery = () => {
  return (
    <StyledGalery>
      <Navbar color={"#000000"} backgroundColor={"#ffffff"} />
      <PageTitle
        color={"#000000"}
        backgroundColor={"#ffffff"}
        mainTitle={"Gallery"}
        descTitle={"僕の視覚的な旅"}
      />
      <ImageList />
    </StyledGalery>
  );
};

export default Gallery;
