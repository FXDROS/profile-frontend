import React from "react";
import axios from "axios";
import Images from "../../component/Images/index";

class ImageList extends React.Component {
  state = {
    toRender: [],
  };

  componentDidMount() {
    axios.get("http://dionisiusamudra.herokuapp.com/api/image/").then((res) =>
      this.setState({
        toRender: res.data,
      })
    );
  }

  render() {
    return <Images data={this.state.toRender} />;
  }
}

export default ImageList;
