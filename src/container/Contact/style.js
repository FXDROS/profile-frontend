import styled from "styled-components";

export const StyledContact = styled.div`
  background-color: rgb(24, 24, 24);

  .contactContent {
    display: flex;
    justify-content: space-evenly;
    align-items: flex-start;
    padding: 10% 5% 10% 5%;
  }

  .showRoom {
    padding: 3%;
    margin-top: 3%;
    width: 90%;
    height: 44rem;
    overflow-y: scroll;
    background-color: rgb(255, 255, 255);
    color: rgb(0, 0, 0);
    margin-bottom: 9%;
  }

  .formContact,
  .formComment {
    font-family: "Montserrat", sans-serif;
    color: white;
    font-size: 125%;
    width: 100%;
    padding: 0 3% 0 3%;
  }
`;
