import React from "react";
import { StyledContact } from "./style";
import PageTitle from "../../component/PageTitle/index";
import Navbar from "../../component/Navbar/index";
import MessageList from "../MessageList/index";
import Forms from "../Forms/index";

const Contact = () => {
  return (
    <StyledContact>
      <Navbar backgroundColor={"rgb(24, 24, 24)"} color={"#ffffff"} />
      <PageTitle
        mainTitle={"Contact"}
        descTitle={"僕に連絡して"}
        color={"#ffffff"}
        backgroundColor={"rgb(24, 24, 24)"}
      />
      <div className="contactContent">
        <div className="formContact">
          <Forms />
        </div>
        <div className="formComment">
          Chat Room / トークルーム
          <br />
          <div className="showRoom">
            <MessageList />
          </div>
        </div>
      </div>
    </StyledContact>
  );
};

export default Contact;
