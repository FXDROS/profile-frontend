import styled from "styled-components";

export const StyledForms = styled.div`
  .formA {
    margin-top: 3%;
    padding: 3% 3% 3% 3%;
    width: 90%;
    font-size: 100%;
    font-weight: 300;
  }
  .formAX {
    margin-top: 3%;
    padding: 3% 3% 3% 3%;
    width: 90%;
    font-size: 100%;
    font-weight: 300;
    height: 10rem;
  }

  .formContact {
    border-right: 2px white solid;
  }

  .formInput {
    margin-bottom: 9%;
    font-weight: 400;
  }
  .submitButton {
    width: 45%;
    border-radius: 10px;
    cursor: pointer;
    border: none;
    padding: 2% 2% 2% 2%;
    margin: 3% 0;
    background-color: white;
    color: black;
    font-size: 90%;
  }
  .submission {
    display: flex;
    justify-content: center;
  }
  .submitButton:hover {
    background-color: rgb(19, 145, 145);
    color: white;
  }
`;
