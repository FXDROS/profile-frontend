import React from "react";
import { StyledForms } from "./style";
import axios from "axios";

// const FormItem = Form.Item;

class Forms extends React.Component {
  handleFormSubmit = (event) => {
    event.preventDefault();
    const name = event.target.elements.nama.value;
    const number = event.target.elements.telpon.value;
    const email = event.target.elements.email.value;
    const message = event.target.elements.pesan.value;

    // console.log(name, number);
    axios
      .post("http://dionisiusamudra.herokuapp.com/api/message/", {
        name: name,
        phone: number,
        email: email,
        content: message,
      })
      .then((res) => console.log(res))
      .catch((error) => console.error(error));
  };

  render() {
    const refreshPage = () => {
      window.location.reload(false);
    };

    return (
      <StyledForms>
        <form onSubmit={this.handleFormSubmit} className="formContact">
          <div className="formInput">
            Name / 名前
            <br />
            <input
              className="formA"
              type="text"
              name="nama"
              placeholder="例）Dionisius Samudra"
              required
            />
          </div>
          <div className="formInput">
            Phone Number / 電話番号
            <br />
            <input
              className="formA"
              type="text"
              name="telpon"
              placeholder="例）08000000000"
              required
            />
          </div>
          <div className="formInput">
            Email Address / 電子メールアドレス
            <br />
            <input
              className="formA"
              type="text"
              name="email"
              placeholder="例）info@dionisiusamudra.com"
              required
            />
          </div>
          <div className="formInput">
            Message / メッセージ
            <br />
            <textarea
              className="formAX"
              type="text"
              name="pesan"
              placeholder="Write your message here..."
              required
            ></textarea>
          </div>
          <div className="submission">
            <input
              type="submit"
              className="submitButton"
              value="send"
              onClick={refreshPage}
            />
          </div>
        </form>
      </StyledForms>
    );
  }
}

export default Forms;
