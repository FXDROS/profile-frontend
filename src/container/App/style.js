import styled from "styled-components";
import Montserrat from "../../assets/fonts/montserrat/Montserrat-Regular.otf";
import MontserratMedium from "../../assets/fonts/montserrat/Montserrat-Medium.otf";
import MontserratBold from "../../assets/fonts/montserrat/Montserrat-ExtraBold.otf";
import MontserratSemiBold from "../../assets/fonts/montserrat/Montserrat-SemiBold.otf";
import MontserratBlack from "../../assets/fonts/montserrat/Montserrat-Black.otf";
import Oxygen from "../../assets/fonts/Oxygen/Oxygen-Regular.ttf";
import OxygenBold from "../../assets/fonts/Oxygen/Oxygen-Bold.ttf";

export const StyledApp = styled.div`
  margin: 0;

  @font-face {
    font-family: "Montserrat";
    src: url(${Montserrat});
    font-weight: normal;
  }

  @font-face {
    font-family: "MontserratBold";
    src: url(${MontserratBold});
    font-weight: normal;
  }

  @font-face {
    font-family: "MontserratSemiBold";
    src: url(${MontserratSemiBold});
    font-weight: normal;
  }

  @font-face {
    font-family: "MontserratBlack";
    src: url(${MontserratBlack});
    font-weight: normal;
  }

  @font-face {
    font-family: "MontserratMedium";
    src: url(${MontserratMedium});
    font-weight: normal;
  }

  @font-face {
    font-family: "Oxygen";
    src: url(${Oxygen});
    font-weight: normal;
  }

  @font-face {
    font-family: "OxygenBold";
    src: url(${OxygenBold});
    font-weight: normal;
  }
`;
