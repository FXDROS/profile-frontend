import React from "react";
import Hobbies from "../Hobbies/index";
import History from "../History/index";
import Interest from "../Interests/index";
import Contact from "../Contact/index";
import { StyledApp } from "./style";
import About from "../About/index";
import LandingPage from "../LandingPage/index";
import { BrowserRouter as Router, Route } from "react-router-dom";
import Gallery from "../Gallery/index";

function App() {
  return (
    <Router>
      <StyledApp>
        <Route exact component={LandingPage} path="/" />
        <Route exact component={About} path="/about" />
        <Route exact component={Hobbies} path="/about/hobbies" />
        <Route exact component={Interest} path="/about/interest" />
        <Route exact component={History} path="/about/history" />
        <Route exact component={Contact} path="/contact" />
        <Route exact component={Gallery} path="/gallery" />
      </StyledApp>
    </Router>
  );
}

export default App;
