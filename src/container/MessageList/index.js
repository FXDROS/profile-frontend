import React from "react";
import axios from "axios";
import Message from "../../component/Message";

class MessageList extends React.Component {
  state = {
    messages: [],
  };

  componentDidMount() {
    axios.get("http://dionisiusamudra.herokuapp.com/api/message/").then((res) =>
      this.setState(
        {
          messages: res.data,
        },
        console.log(res.data)
      )
    );
  }

  render() {
    return <Message data={this.state.messages} />;
  }
}

export default MessageList;

//36.09
