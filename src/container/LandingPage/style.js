import styled from "styled-components";

export const StyledLanding = styled.div`
  header {
    background: linear-gradient(
      -45deg,
      rgb(252, 52, 152),
      rgb(232, 106, 240),
      rgb(30, 187, 215),
      rgb(0, 210, 127)
    );
    background-size: 400% 400%;
    width: 100%;
    height: 100vh;
    animation: moving 10s ease-in-out;
    animation-iteration-count: infinite;
    z-index: 10;
  }

  .content {
    height: 100vh;
    display: flex;
    justify-content: center;
    align-items: center;
  }
  .arrow {
    display: flex;
    justify-content: center;
    align-items: center;
  }
  .arrow a {
    position: absolute;
    bottom: 5%;
    animation: bouncing 2s;
    animation-iteration-count: infinite;
  }

  .message {
    font-family: "OxygenBold", sans-serif;
    font-size: 600%;
    color: white;
    border-right: 3px solid white;
    animation: typing 3s steps(18);
    overflow: hidden;
    white-space: nowrap;
    width: 18ch;
  }

  #about {
    width: 100%;
    display: flex;
    justify-content: flex-start;
    align-content: center;
    align-items: center;
    font-family: "Oxygen", sans-serif;
    color: white;
    background-color: black;
    padding-top: 10%;
    padding-bottom: 5%;
    overflow: hidden;
  }
  #about p {
    font-family: "montserrat", sans-serif;
    font-size: 125%;
    line-height: 150%;
  }
  .aboutMessage {
    border-left: solid white 6px;
    padding: 0 10% 0 3%;
    z-index: 1;
  }

  #about h1 {
    font-size: 500%;
    font-weight: 600;
    padding-top: 10%;
  }

  @keyframes moving {
    0% {
      background-position: 0% 50%;
    }
    50% {
      background-position: 100% 50%;
    }
    100% {
      background-position: 0% 50%;
    }
  }
  @keyframes typing {
    0% {
      width: 0ch;
    }
    100% {
      width: 18ch;
    }
  }
  @keyframes bouncing {
    0% {
      bottom: 5%;
    }
    50% {
      bottom: 10%;
    }
    100% {
      bottom: 5%;
    }
  }
`;
