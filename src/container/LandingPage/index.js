import React, { useLayoutEffect, useState } from "react";
import { StyledLanding } from "./style";
import Navbar from "../../component/Navbar/index";
import Profile from "../../assets/images/Profile Photo.001.jpeg";
import AboutMe from "../../assets/images/Slide1.jpeg";

const LandingPage = () => {
  const [backgroundColor, setIsScrolled] = useState("transparent");

  useLayoutEffect(() => {
    window.addEventListener("scroll", onScroll);
  }, [backgroundColor]);

  const onScroll = () => {
    const transition = window.scrollY * 100;
    if (transition > 10) {
      setIsScrolled("#000000");
    } else {
      setIsScrolled("transparent");
    }
  };

  return (
    <StyledLanding>
      <Navbar color={"#ffffff"} backgroundColor={backgroundColor} />
      <header>
        <div className="content">
          <div className="message">You Know Who I Am.</div>
        </div>
        <div className="arrow">
          <a href="#about">
            <img
              src="https://img.icons8.com/ios-filled/96/000000/double-down.png"
              style={{ width: "100%" }}
              alt="down"
            />
          </a>
        </div>
      </header>
      <div id="about">
        <div
          style={{
            padding: "5% 0 5% 5%",
            width: "130%",
            zIndex: "1",
            borderTop: "solid white 6px",
            marginRight: "5%",
          }}
        >
          <img src={Profile} width="100%" height="100%" alt="profile" />
        </div>
        <div className="aboutMessage">
          <img
            src={AboutMe}
            style={{
              width: "55%",
              position: "absolute",
              right: "0%",
              marginTop: "-10%",
            }}
            alt="about"
          />
          <h1>Hi There!</h1>
          <p>
            I'm Dionisius Baskoro Samudra, a student in University of Indonesia.
            I'm currently pursuing bachelor degree in computer science major.
            There are lots of stuffs that become my interest, such as Japanese
            culture, computers, video games, and others. One of my favorite
            sport is Uni Hockey. To know more about me, why don't you explore
            this webpage on your own? Cheers!
          </p>
        </div>
      </div>
    </StyledLanding>
  );
};

export default LandingPage;
