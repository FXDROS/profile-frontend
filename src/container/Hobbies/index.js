import React from "react";
import Navbar from "../../component/Navbar/index";
import { StyledHobbies } from "./style";
import PageTitle from "../../component/PageTitle/index";
import Books from "../../assets/images/book.jpeg";
import Game from "../../assets/images/game.jpeg";
import Gundam from "../../assets/images/gundam.jpeg";

const Hobbies = () => {
  return (
    <StyledHobbies>
      <Navbar color={"#000000"} backgroundColor={"#ffffff"} />
      <PageTitle
        color={"black"}
        mainTitle={"Hobbies"}
        descTitle={"僕の趣味のすべて"}
        backgroundColor={"white"}
      />
      <div className="row">
        <div className="columnleft">
          <img src={Books} width="100%" height="100%" alt="books" />
        </div>
        <div className="columnright">
          <h1>Reading</h1>
          <p>
            I would like being honest that, I could be one of those people who
            hate to read books. I only read text books for study purpose,
            nothing less nothing more. Yes, it was really boring to read those
            realistic and factual stuffs. Maybe, that's why I hate to read at
            that time. But one day, one of my friends persuaded me to read
            fictional books everyday and I eventually give up. I didn't resist
            and took a book from the bookshelf. I sit down, read the first page
            and didn't even move until I finished it.
          </p>
        </div>
      </div>
      <div className="row" style={{ marginTop: "6%" }}>
        <div className="columnright" style={{ textAlign: "right" }}>
          <h1>Games</h1>
          <p>
            No game no life. Yes I know, it's a title of an anime opening song.
            But, I suppose, it really make sense. This life is full of games.
            Even you can play with politics haha. Well, let's not discuss that
            part. I do remember my first gaming console is Gameboy Advance by
            Nintendo. My father gave it to me when I was four. Well, it was
            really satisfying to play in such low screen resolution that day.
            But now, we have 4K graphics that makes gaming to the next level,
            not forget to mention those triple As games. One of my favorite is
            Assassin's Creed Syndicate.
          </p>
        </div>
        <div className="columnleft">
          <img src={Game} width="100%" height="100%" alt="games" />
        </div>
      </div>
      <div className="row" style={{ marginBottom: "5%", marginTop: "6%" }}>
        <div className="columnleft">
          <img src={Gundam} width="100%" height="100%" alt="gundam" />
        </div>
        <div className="columnright">
          <h1>Action Figure</h1>
          <p>
            Kids play with toys, but grown ups play with acion figures. Well, I
            should admit that they are pretty similar. But, the things that I
            would like to mention here are no other than Gundam. Well, at first
            me, myself not really into it. I don't really know and I don't
            really care. But this one anime called Little Battlers eXperiece
            (LBX) "hypnotized" me. I started to buy some of the model kits.
            Until one day, when I've collected lots of it, I started to think
            "should I try to assemble a Gundam next time?" Well, it was a
            horrible decision because now I can't get myself away from it. Yes,
            it is seriously addicting.
          </p>
        </div>
      </div>
    </StyledHobbies>
  );
};

export default Hobbies;
