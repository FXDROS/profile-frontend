import styled from "styled-components";

export const StyledHobbies = styled.div`
  .columnleft {
    width: 30%;
    float: left;
  }
  .row {
    margin: 10% 0 0 5%;
    font-family: "MontserratMedium", sans-serif;
    display: table;
    clear: both;
    content: "";
  }
  .row p {
    font-size: 125%;
    line-height: 150%;
  }
  .columnright {
    width: 55%;
    margin: 0 5% 0 5%;
    float: left;
  }
  .columnright h1 {
    font-size: 375%;
    font-weight: 500;
  }
`;
