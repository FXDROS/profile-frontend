import styled from "styled-components";

export const StyledAbout = styled.div`
  .about2 {
    display: flex;
    justify-content: center;
    font-family: "MontserratMedium", sans-serif;
  }
  .hobby,
  .interest,
  .history {
    width: 100%;
    border-bottom: rgb(102, 102, 102);
    display: flex;
    justify-content: center;
    align-items: center;
  }
  .interest {
    border-right: solid 2px rgba(102, 102, 102);
    border-left: solid 2px rgba(102, 102, 102);
  }
  .overlay {
    background-color: rgba(0, 0, 0, 0.7);
    height: 100%;
    width: 100%;
    position: absolute;
    top: 0;
    bottom: 0;
    right: 0;
    left: 0;
  }
  .iimage,
  .himage,
  .timage {
    opacity: 0;
    width: 100%;
    display: flex;
    justify-content: center;
    position: relative;
  }
  .interest:hover .iimage,
  .hobby:hover .himage,
  .history:hover .timage {
    opacity: 1;
    transition: 0.5s ease-in-out;
    color: white;
  }

  .hobby:hover a,
  .interest:hover a,
  .history:hover a {
    color: white;
  }
  .hobby:hover .textdesc,
  .interest:hover .textdesc,
  .history:hover .textdesc {
    color: white;
  }

  .hobby:hover .pretext,
  .interest:hover .pretext,
  .history:hover .pretext {
    opacity: 0;
  }

  .text {
    position: absolute;
    font-size: 400%;
    font-weight: 500;
  }
  .text a {
    color: black;
    text-decoration: none;
  }
  .textdesc {
    font-size: 25%;
    color: rgba(0, 0, 0, 0);
    font-weight: 400;
    text-align: center;
    line-height: 150%;
  }
  .pretext {
    font-size: 25%;
    color: black;
    font-weight: 400;
    text-align: center;
    line-height: 150%;
  }
`;
