import React from "react";
import Navbar from "../../component/Navbar/index";
import { StyledAbout } from "./style";
import Hobby from "../../assets/images/hobby.jpeg";
import Interest from "../../assets/images/interest.jpeg";
import History from "../../assets/images/history.jpeg";
import { Link } from "react-router-dom";

const About = () => {
  return (
    <StyledAbout>
      <Navbar color={"#ffffff"} backgroundColor={"#000000"} />
      <div className="about2">
        <Link to="/about/hobbies">
          <div className="hobby">
            <div className="himage">
              <img src={Hobby} width="100%" height="100%" alt="hobbies" />
              <div className="overlay"></div>
            </div>
            <div className="text">
              <div className="pretext">A way of wasting time,</div>
              <a href="hobbies">Hobby</a>
              <div className="textdesc">
                Everything I do in my
                <br />
                free or stressed time.
              </div>
            </div>
          </div>
        </Link>
        <Link to="/about/interest">
          <div className="interest">
            <div className="iimage">
              <img src={Interest} width="100%" height="100%" alt="interest" />
              <div className="overlay"></div>
            </div>
            <div className="text">
              <div className="pretext">Looking for passion,</div>
              <a href="interest">Interest</a>
              <div className="textdesc">
                Things that turn me on
                <br />
                everytime I see them.
              </div>
            </div>
          </div>
        </Link>
        <Link to="/about/history">
          <div className="history">
            <div className="timage">
              <img src={History} width="100%" height="100%" alt="history" />
              <div className="overlay"></div>
            </div>
            <div className="text">
              <div className="pretext">A story not to remember,</div>
              <a href="history">History</a>
              <div className="textdesc">
                All about my past,
                <br />
                some maybe shameful.
              </div>
            </div>
          </div>
        </Link>
      </div>
    </StyledAbout>
  );
};

export default About;
