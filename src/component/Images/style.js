import styled from "styled-components";

export const StyledImages = styled.div`
  display: flex;
  justify-content: center;
  align-items: flex-start;
  margin: 10% 5% 10% 5%;

  .photos {
    width: 30%;
  }

  .image {
    padding: 1.5%;
  }
`;
