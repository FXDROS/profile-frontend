import React from "react";
import { StyledImages } from "./style";

const Images = (props) => {
  const index = props.data;

  return (
    <StyledImages>
      <div className="photos">
        {index.map((item, index) =>
          index % 3 === 0 ? (
            <div className="image">
              <img src={item.image} alt={"from-api"} width="100%" />
            </div>
          ) : (
            <div />
          )
        )}
      </div>
      <div className="photos">
        {index.map((item, index) =>
          index % 3 === 1 ? (
            <div className="image">
              <img src={item.image} alt={"from-api"} width="100%" />
            </div>
          ) : (
            <div />
          )
        )}
      </div>
      <div className="photos">
        {index.map((item, index) =>
          index % 3 === 2 ? (
            <div className="image">
              <img src={item.image} alt={"from-api"} width="100%" />
            </div>
          ) : (
            <div />
          )
        )}
      </div>
    </StyledImages>
  );
};

export default Images;
