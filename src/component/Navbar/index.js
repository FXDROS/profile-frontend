import React from "react";
import { StyledNav } from "./style";
import { Link } from "react-router-dom";

const Navbar = ({ color, backgroundColor }) => {
  return (
    <StyledNav backgroundColor={backgroundColor} color={color}>
      <nav>
        <div className="logo">
          <Link to="/" className="ds">
            DS
          </Link>
        </div>
        <div className="menu">
          <Link to="/about" className="under-border">
            About
          </Link>
          <Link to="/contact" className="under-border">
            Contact
          </Link>
          <Link to="/gallery" className="under-border">
            Gallery
          </Link>
        </div>
      </nav>
    </StyledNav>
  );
};

export default Navbar;
