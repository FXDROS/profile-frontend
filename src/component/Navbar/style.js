import styled from "styled-components";

export const StyledNav = styled.div`
  :root {
    --color1: transparent;
    --color2: rgba(0, 0, 0, 1);
  }
  nav {
    width: 100%;
    background-color: ${(props) => props.backgroundColor};
    position: fixed;
    top: 0%;
    left: 0%;
    display: flex;
    justify-content: space-between;
    padding: 2% 0 2% 0;
    transition: all 0.3s ease-out;
    z-index: 10;
  }
  nav a {
    color: ${(props) => props.color};
    text-decoration: none;
    font-size: 180%;
    font-family: "MontserratSemiBold", sans-serif;
    position: relative;
  }
  .logo {
    padding-left: 1.5%;
  }
  .menu {
    display: flex;
    min-width: 25%;
    justify-content: space-between;
    padding-right: 3%;
  }

  .under-border::after {
    content: "";
    width: 0%;
    height: 3px;
    background: ${(props) => props.color};
    position: absolute;
    bottom: 0%;
    left: 50%;
    border-radius: 10px;
    transition: all 500ms ease;
  }
  .under-border:hover::after {
    left: 0%;
    width: 100%;
  }
  .ds {
    border: 5px solid ${(props) => props.color};
    font-family: "OxygenBold", sans-serif;
    padding: 8px;
    font-size: 250%;
  }
`;
