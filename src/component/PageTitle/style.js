import styled from "styled-components";

export const StyledTitle = styled.div`
  background: ${(props) => props.backgroundColor};
  color: ${(props) => props.color};

  .title {
    font-family: "MontserratSemiBold", sans-serif;
    font-size: 450%;
    font-weight: 600;
    line-height: 0%;
    padding: 15% 0% 1.5% 5%;
    width: 8ch;
  }
  .title h6 {
    font-size: 35%;
    padding-top: 3%;
    font-family: "Montserrat", sans-serif;
  }
`;
