import React from "react";
import { StyledTitle } from "./style";

const PageTitle = ({ color, mainTitle, descTitle, backgroundColor }) => {
  return (
    <StyledTitle color={color} backgroundColor={backgroundColor}>
      <div className="title">
        {mainTitle}
        <h6>{descTitle}</h6>
      </div>
    </StyledTitle>
  );
};

export default PageTitle;
