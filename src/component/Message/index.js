import React from "react";
import { StyledMessage } from "./style";
import { List } from "antd";

const Message = (props) => {
  return (
    <StyledMessage>
      {props.data.length !== 0 ? (
        <List
          dataSource={props.data}
          renderItem={(item) => (
            <List.Item key={item.title} className="innerList">
              <h5>{item.name}</h5>
              <p>{item.content}</p>
            </List.Item>
          )}
        />
      ) : (
        <div />
      )}
    </StyledMessage>
  );
};

export default Message;
