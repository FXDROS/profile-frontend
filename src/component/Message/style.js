import styled from "styled-components";

export const StyledMessage = styled.div`
  padding-top: 2rem;

  .innerList {
    list-style-type: none;
    margin: -1.5rem 0 0 -2rem;
  }

  .innerList p {
    margin: -3.5% 0 8% 0;
    font-family: "Montserrat", sans-serif;
    font-size: 100%;
  }

  .innerList h5 {
    font-family: "MontserratSemiBold", sans-serif;
    font-size: 75%;
  }
`;
